path_1 = "text.txt"
path_2 = "text_2.txt"

with open(path_1, 'rt') as f1, open(path_2, "at") as f2:
    print(f"f.1  tell result: {f1.tell()}")
    str_1 = f1.readlines()
    print(f"f.1  tell result: {f1.tell()}")
    f2.writelines(reversed(str_1))
