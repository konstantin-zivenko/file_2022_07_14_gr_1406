# .read(size=-1)

with open("text.txt", "rt") as f:
    print(f.read())

# .readline(size=-1)

with open("text.txt", "rt") as f:
    print(f.readline())


with open("text.txt", "rt") as f:
    line = f.readline()
    while line != '':
        print(line, end='')
        line = f.readline()

# .readlines()

with open("text.txt", "rt") as f:
    print(f.readlines())