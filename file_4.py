# .write(string)

with open("text.txt", "rt") as f:
    all_string = f.readlines()

with open("text_2.txt", "wt") as f:
    for string in reversed(all_string):
        f.write(string)

# .writelines(seq)